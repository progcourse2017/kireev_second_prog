#include <iostream>
using namespace std;

int main() {
	double x;
	
	cout << "Enter x: ";
	cin >> x;

	if (x <= -2 || x >= 2)
		cout << 0 << endl;
	else if (x > -2 && x < -1)
		cout << -x - 2 << endl;
	else if (x >= -1 && x <= 1)
		cout << x << endl;
	else if (x < 2 && x > 1)
		cout << -x + 2 << endl;

	system("pause");
	return 0;
}